package mbk.firstProject.firstProject;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
@SpringBootApplication
public class BuildLogAppplication {

    public static void main(String[] args) {
        SpringApplication.run(BuildLogAppplication.class, args);
    }
}
