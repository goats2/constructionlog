package mbk.firstProject.firstProject.repository;

import mbk.firstProject.firstProject.entity.Acceptancelevel;
import org.springframework.data.repository.CrudRepository;

public interface AcceptancelevelRepository extends CrudRepository <Acceptancelevel, Integer> {
}
