package mbk.firstProject.firstProject.repository;

import mbk.firstProject.firstProject.entity.Ticket;
import org.springframework.data.repository.CrudRepository;

public interface TicketRepository extends CrudRepository <Ticket, Integer> {
}
