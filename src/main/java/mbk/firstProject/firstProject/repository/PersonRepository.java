package mbk.firstProject.firstProject.repository;


import mbk.firstProject.firstProject.entity.Person;
import org.springframework.data.repository.CrudRepository;


public interface PersonRepository extends CrudRepository<Person, String> {

}
