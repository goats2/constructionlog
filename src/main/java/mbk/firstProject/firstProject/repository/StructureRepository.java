package mbk.firstProject.firstProject.repository;

import mbk.firstProject.firstProject.entity.Structure;
import org.springframework.data.repository.CrudRepository;

public interface StructureRepository extends CrudRepository<Structure, Integer> {
}
