package mbk.firstProject.firstProject.service;

import mbk.firstProject.firstProject.entity.Person;
import mbk.firstProject.firstProject.repository.PersonRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class Access {

    private PersonRepository personRepository;

    @Autowired
    public Access(PersonRepository personRepository) {
        this.personRepository = personRepository;
    }

    private Person accessForPerson(String login) {

        if(personRepository.existsById(login)) {
            Person person = personRepository.findById(login).get();
            return person;
        } else {
            System.out.println("Zły login");
        }
        return null;
    }

    private void choiceTypeAccess (Person person){
        if(person.getFunction().equals("Supervisor")){
            /*TODO
            *dostęp dla nadzoru
            * brak możliości edytowania wpisów kierownika */
        } else if(person.getFunction().equals("ConstructionManager")){
            /*TODO
            *dostep dla wykonawcy
            * brak możliwości edytowania wpisów nadzoru */
        } else {
            /*TODO
            * przewidzieć błąd w Function*/
        }

    }

    public void downloadDataAccess(String Login, String Password) {
        if(accessForPerson(Login).equals(null)){
            System.out.println("Ponów prubę!");
        } else if (accessForPerson(Login).getPassword().equals(Password)){
            Person person = accessForPerson(Login);
            System.out.println("Witaj " + person.getName() + " " + person.getSurname());
            choiceTypeAccess(person);
        } else {
            System.out.println("Błędne hasło!");
        }

    }
}
