package mbk.firstProject.firstProject.entity;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Set;


@Entity
@Table(name = "ticket")
public class Ticket {

    @Id
    @GeneratedValue (strategy = GenerationType.IDENTITY)
    @Column (name = "id")
    private Integer ticketId;
    @Column (name = "typeticket")
    private String typeTicket;

    @ManyToOne
    @JoinColumn(name = "structureId")
    private Structure structure;
    @ManyToOne
    @JoinColumn(name = "roadsId")
    private Roads roads;

    private BigDecimal ticketRoadStart;
    private BigDecimal ticketRoadStop;

    @ManyToOne
    @JoinColumn (name = "constructionManagerId")
    private Person constructionManager;

    private String comments;

    @OneToMany(mappedBy = "ticket", cascade = CascadeType.ALL)
    private Set<Ticket> ticketSet;

    public Ticket(String typeTicket, Structure structure, Roads roads, BigDecimal ticketRoadStart, BigDecimal ticketRoadStop, Person constructionManager, String comments) {
        this.typeTicket = typeTicket;
        this.structure = structure;
        this.roads = roads;
        this.ticketRoadStart = ticketRoadStart;
        this.ticketRoadStop = ticketRoadStop;
        this.constructionManager = constructionManager;
        this.comments = comments;
    }

    public Ticket () {};

    public Integer getTicketId() {
        return ticketId;
    }

    public void setTicketId(Integer ticketId) {
        this.ticketId = ticketId;
    }

    public String getTypeTicket() {
        return typeTicket;
    }

    public void setTypeTicket(String typeTicket) {
        this.typeTicket = typeTicket;
    }

    public Structure getStructure() {
        return structure;
    }

    public void setStructure(Structure structure) {
        this.structure = structure;
    }

    public Roads getRoads() {
        return roads;
    }

    public void setRoads(Roads roads) {
        this.roads = roads;
    }

    public BigDecimal getTicketRoadStart() {
        return ticketRoadStart;
    }

    public void setTicketRoadStart(BigDecimal ticketRoadStart) {
        this.ticketRoadStart = ticketRoadStart;
    }

    public BigDecimal getTicketRoadStop() {
        return ticketRoadStop;
    }

    public void setTicketRoadStop(BigDecimal ticketRoadStop) {
        this.ticketRoadStop = ticketRoadStop;
    }

    public Person getConstructionManager() {
        return constructionManager;
    }

    public void setConstructionManager(Person constructionManager) {
        this.constructionManager = constructionManager;
    }

    public String getComments() {
        return comments;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }

    public Set<Ticket> getTicketSet() {
        return ticketSet;
    }

    public void setTicketSet(Set<Ticket> ticketSet) {
        this.ticketSet = ticketSet;
    }

    @Override
    public String toString() {
        return "Ticket{" +
                "ticketId=" + ticketId +
                ", typeTicket='" + typeTicket + '\'' +
                ", structure=" + structure +
                ", roads=" + roads +
                ", ticketRoadStart=" + ticketRoadStart +
                ", ticketRoadStop=" + ticketRoadStop +
                ", constructionManager=" + constructionManager +
                ", comments='" + comments + '\'' +
                '}';
    }
}
