package mbk.firstProject.firstProject.entity;

import javax.persistence.*;
import java.util.Set;


@Entity
@Table(name="person")
public class Person {

    @Id
    @Column(name="personLogin")
    private String login;
    @Column(name="personPassword")
    private String password;
    @Column(name="personFunction")
    private String function;
    @Column(name="personName")
    private String name;
    @Column(name="personSurname")
    private String surname;
    @Column(name="personLicence")
    private String licenceNumber;
    private String additionalInformation;
    @OneToMany(mappedBy = "supervisor", cascade = CascadeType.ALL)
    private Set<Acceptancelevel> acceptancelevelSet;
    @OneToMany(mappedBy = "constructionManager", cascade = CascadeType.ALL)
    private  Set<Ticket> ticketSet;


    public Person(String login, String password, String function, String name, String surname, String licenceNumber, String additionalInformation) {
        this.login = login;
        this.password = password;
        this.function = function;
        this.name = name;
        this.surname = surname;
        this.licenceNumber = licenceNumber;
        this.additionalInformation = additionalInformation;
    }

    public Person() {}

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getName() {
        return name;
    }

    public void setName(String nameSupervisor) {
        this.name = nameSupervisor;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surnameSupervisor) {
        this.surname = surnameSupervisor;
    }

    public String getLicenceNumber() {
        return licenceNumber;
    }

    public void setLicenceNumber(String licenceNumber) {
        this.licenceNumber = licenceNumber;
    }

    public String getAdditionalInformation() {
        return additionalInformation;
    }

    public void setAdditionalInformation(String additionalInformation) {
        this.additionalInformation = additionalInformation;
    }

    public String getFunction() {
        return function;
    }

    public void setFunction(String function) {
        this.function = function;
    }

    public Set<Acceptancelevel> getAcceptancelevelSet() {
        return acceptancelevelSet;
    }

    public void setAcceptancelevelSet(Set<Acceptancelevel> acceptancelevelSet) {
        this.acceptancelevelSet = acceptancelevelSet;
    }

    public Set<Ticket> getTicketSet() {
        return ticketSet;
    }

    public void setTicketSet(Set<Ticket> ticketSet) {
        this.ticketSet = ticketSet;
    }



    @Override
    public String toString() {
        return "Person{" +
                "login='" + login + '\'' +
                ", password='" + password + '\'' +
                ", name='" + name + '\'' +
                ", surname='" + surname + '\'' +
                ", licenceNumber='" + licenceNumber + '\'' +
                ", additionalInformation='" + additionalInformation + '\'' +
                '}';
    }
}
