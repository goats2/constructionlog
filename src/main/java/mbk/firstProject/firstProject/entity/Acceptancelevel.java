package mbk.firstProject.firstProject.entity;

import javax.persistence.*;
import java.math.BigDecimal;

@Entity
@Table (name = "acceptancelevel")
public class Acceptancelevel {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer acceptancelevelId;
    @ManyToOne
    @JoinColumn(name = "supervisorId")
    private Person supervisor;

    @ManyToOne
    @JoinColumn(name = "ticketId")
    private Ticket ticket;

    private boolean acceptance;
    private BigDecimal acceptanceRoadStart;
    private BigDecimal acceptanceRoadEnd;
    private String comments;

    public Acceptancelevel(Person supervisor, Ticket ticket, boolean acceptance, BigDecimal acceptanceRoadStart, BigDecimal acceptanceRoadEnd, String comments) {
        this.supervisor = supervisor;
        this.ticket = ticket;
        this.acceptance = acceptance;
        this.acceptanceRoadStart = acceptanceRoadStart;
        this.acceptanceRoadEnd = acceptanceRoadEnd;
        this.comments = comments;
    }

    public Acceptancelevel() {}

    public Integer getAcceptancelevelId() {
        return acceptancelevelId;
    }

    public void setAcceptancelevelId(Integer acceptancelevelId) {
        this.acceptancelevelId = acceptancelevelId;
    }

    public Person getSupervisor() {
        return supervisor;
    }

    public void setSupervisor(Person supervisor) {
        this.supervisor = supervisor;
    }

    public Ticket getTicket() {
        return ticket;
    }

    public void setTicket(Ticket ticket) {
        this.ticket = ticket;
    }

    public boolean isAcceptance() {
        return acceptance;
    }

    public void setAcceptance(boolean acceptance) {
        this.acceptance = acceptance;
    }

    public BigDecimal getAcceptanceRoadStart() {
        return acceptanceRoadStart;
    }

    public void setAcceptanceRoadStart(BigDecimal acceptanceRoadStart) {
        this.acceptanceRoadStart = acceptanceRoadStart;
    }

    public BigDecimal getAcceptanceRoadEnd() {
        return acceptanceRoadEnd;
    }

    public void setAcceptanceRoadEnd(BigDecimal acceptanceRoadEnd) {
        this.acceptanceRoadEnd = acceptanceRoadEnd;
    }

    public String getComments() {
        return comments;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }

    @Override
    public String toString() {
        return "Acceptancelevel{" +
                "acceptancelevelId=" + acceptancelevelId +
                ", supervisor=" + supervisor +
                ", ticket=" + ticket +
                ", acceptance=" + acceptance +
                ", acceptanceRoadStart=" + acceptanceRoadStart +
                ", acceptanceRoadEnd=" + acceptanceRoadEnd +
                ", comments='" + comments + '\'' +
                '}';
    }
}
