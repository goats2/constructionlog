package mbk.firstProject.firstProject.entity;


import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Set;

@Entity
@Table(name = "roads")
public class Roads {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer roadsId;

    private String roadName;
    private String roadType;
    private BigDecimal roadLenght;
    private BigDecimal roadStart;
    private BigDecimal roadEnd;
    private Integer numberLayer;
    @ManyToOne
    @JoinColumn(name = "structureId")
    private Structure structure;
    @OneToMany(mappedBy = "roads",cascade = CascadeType.ALL)
    private Set<Ticket> ticketSet;

    public Roads(String roadName, String roadType, BigDecimal roadLenght, BigDecimal roadStart, BigDecimal roadEnd, Integer numberLayer, Structure structure) {
        this.roadName = roadName;
        this.roadType = roadType;
        this.roadLenght = roadLenght;
        this.roadStart = roadStart;
        this.roadEnd = roadEnd;
        this.numberLayer = numberLayer;
        this.structure = structure;
    }

    public Roads () {}

    public Integer getRoadsId() {
        return roadsId;
    }

    public void setRoadsId(Integer roadsId) {
        this.roadsId = roadsId;
    }

    public String getRoadName() {
        return roadName;
    }

    public void setRoadName(String roadName) {
        this.roadName = roadName;
    }

    public String getRoadType() {
        return roadType;
    }

    public void setRoadType(String roadType) {
        this.roadType = roadType;
    }

    public BigDecimal getRoadLenght() {
        return roadLenght;
    }

    public void setRoadLenght(BigDecimal roadLenght) {
        this.roadLenght = roadLenght;
    }

    public BigDecimal getRoadStart() {
        return roadStart;
    }

    public void setRoadStart(BigDecimal roadStart) {
        this.roadStart = roadStart;
    }

    public BigDecimal getRoadEnd() {
        return roadEnd;
    }

    public void setRoadEnd(BigDecimal roadEnd) {
        this.roadEnd = roadEnd;
    }

    public Integer getNumberLayer() {
        return numberLayer;
    }

    public void setNumberLayer(Integer numberLayer) {
        this.numberLayer = numberLayer;
    }

    public Structure getStructure() {
        return structure;
    }

    public void setStructure(Structure structure) {
        this.structure = structure;
    }

    public Set<Ticket> getTicketSet() {
        return ticketSet;
    }

    public void setTicketSet(Set<Ticket> ticketSet) {
        this.ticketSet = ticketSet;
    }

    @Override
    public String toString() {
        return "Roads{" +
                "roadsId=" + roadsId +
                ", roadName='" + roadName + '\'' +
                ", roadType='" + roadType + '\'' +
                ", roadLenght=" + roadLenght +
                ", roadStart=" + roadStart +
                ", roadEnd=" + roadEnd +
                ", numberLayer=" + numberLayer +
                ", structure=" + structure +
                '}';
    }
}
