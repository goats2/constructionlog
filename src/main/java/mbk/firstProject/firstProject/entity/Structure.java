package mbk.firstProject.firstProject.entity;

import javax.persistence.*;
import java.util.Set;

@Entity
@Table(name = "structure")
public class Structure {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer structureId;
    private String layer;
    private String thickness;
    @OneToMany(mappedBy = "structure", cascade = CascadeType.ALL)
    private Set<Roads> roadsSet;
    @OneToMany(mappedBy = "structure",cascade = CascadeType.ALL)
    private Set<Ticket> ticketSet;


    public Structure(String layer, String thickness) {
        this.layer = layer;
        this.thickness = thickness;
    }
    public Structure() {}

    public Integer getStructureId() {
        return structureId;
    }

    public void setStructureId(Integer structureId) {
        this.structureId = structureId;
    }

    public String getLayer() {
        return layer;
    }

    public void setLayer(String layer) {
        this.layer = layer;
    }

    public String getThickness() {
        return thickness;
    }

    public void setThickness(String thickness) {
        this.thickness = thickness;
    }

    public Set<Roads> getRoadsSet() {
        return roadsSet;
    }

    public void setRoadsSet(Set<Roads> roadsSet) {
        this.roadsSet = roadsSet;
    }

    public Set<Ticket> getTicketSet() {
        return ticketSet;
    }

    public void setTicketSet(Set<Ticket> ticketSet) {
        this.ticketSet = ticketSet;
    }

    @Override
    public String toString() {
        return "Structure{" +
                "structureId=" + structureId +
                ", layer='" + layer + '\'' +
                ", thickness='" + thickness + '\'' +
                '}';
    }
}
