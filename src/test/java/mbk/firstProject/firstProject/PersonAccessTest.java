package mbk.firstProject.firstProject;


import mbk.firstProject.firstProject.entity.Person;
import mbk.firstProject.firstProject.repository.PersonRepository;
import mbk.firstProject.firstProject.service.Access;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class PersonAccessTest {
    @Autowired
    private PersonRepository personRepository;

    @Autowired
    private Access access;

    @Test
    public void repositoryTest() {
        personRepository.findAll().forEach(person -> System.out.println(person));
    }
    @Test
    public void acceesForSupervisorByLogin(){
        Person person = access.accessForPerson("Login");
        String result = person.getPassword();
        System.out.println("Wynik po loginie "+result);

    }
    @Test
    public void acceesForSupervisorByWrongLogin(){
        Assert.assertNull(access.accessForPerson("Zły login"));
    }
    @Test
    public void downloadDataAccesstest() {
        access.downloadDataAccess("Login","Password");
    }

    @Test
    public void createPerson() {
        personRepository.save(new Person(
                "Login",
                "Password",
                "Supervisor",
                "Name",
                "Surname",
                "licenceNumber",
                "String additionalInformation"));
    }


}
